# Boite à outils, utilitaires divers
Ce dépôt regroupe différent utilitaire écrit en java.

### Générer les programmes
Les programmes sont accessibles dans le dossiers "jars" après execution du script "gen-jar.sh". Ce script génére les classes et jars compatibles avec votre version de java.

## imageToASCII
imageToASCII convertie une image type pixel art en ASCII art coloré. Une explication du fonctionnement du codage couleur ANSI ainsi qu'une liste des émulateurs de ternimal compatible peut être trouvé [ici](https://gist.github.com/XVilka/8346728).


#### Comment lancer le programme ?
imageToASCII est écrit en java et peut être lancé avec la commande suivante :
`java -jar jars/imageToASCII [-preview] [--facteur ...] --output path Image1 Image2 ...`


#### Man page
```
usage: ascii_convert [-preview] [--facteur ...] --output path Image1
                     Image2 ...
    --facteur <facteur>   facteur d'agrandissement de l'image
 -help                    affiche cette aide
    --output <output>     dossier de destination des fichiers
 -preview                 affiche les images afin de vérifie le facteur
                          d'agrandissement avant exportation
```


#### Preview
![preview](https://gitlab.univ-lille.fr/adil.benameur.etu/toolbox/raw/master/imageToASCII/shots/preview.png "preview")


## printFile
printFile affiche le contenu d'un fichier et convertie les codes couleurs ANSI en couleur.


#### Comment lancer le programme ?
printFile est écrit en java et peut être lancé avec la commande suivante :
`java -jar jars/printFile.jar Files...`


#### Preview
![preview](https://gitlab.univ-lille.fr/adil.benameur.etu/toolbox/raw/master/printFile/shots/preview.png "preview")
