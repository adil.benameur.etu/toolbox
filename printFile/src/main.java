import org.apache.tools.ant.DirectoryScanner;

import java.io.*;
import java.util.ArrayList;

public class main {
    public static void main(String[] args) throws IOException {
        String st;

        args = getListArgs(args);

        for (String file : args) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                while ((st = br.readLine()) != null)
                    System.out.println(st);
            } catch (FileNotFoundException exp) {
                System.out.println(exp);
            }


        }
    }

    private static String[] getListArgs(String[] arguments) {
        ArrayList<String> files = new ArrayList<String>();
        DirectoryScanner ds = new DirectoryScanner();

        String absoluteParentDirectory;

        for (String argument : arguments) {
            absoluteParentDirectory = new File(new File(argument).getAbsolutePath()).getParent();

            ds.setIncludes(new String[]{new File(argument).getName()});
            ds.setBasedir(absoluteParentDirectory);
            ds.setCaseSensitive(true);
            ds.scan();

            for (String file: ds.getIncludedFiles()) {
                files.add(absoluteParentDirectory + "/" + file);
            }
        }

        String[] toReturn = new String[files.size()];
        files.toArray(toReturn);

        return toReturn;
    }

}
