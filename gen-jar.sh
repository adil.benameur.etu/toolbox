#!/bin/bash

# Compile les .java des programmes
echo "Compilation des programmes vers out/"

# Pour imageToASCII
javac -classpath libs/ant.jar:libs/commons-cli-1.4.jar:imageToASCII/src/main.java -d out/production/imageToASCII/ imageToASCII/src/main.java
cp -r imageToASCII/src/META-INF out/production/imageToASCII/

# Pour printFile
javac -classpath libs/ant.jar:printFile/src/main.java -d out/production/printFile/ printFile/src/main.java
cp -r printFile/src/META-INF out/production/printFile/


# Création du dossier destination des jars
mkdir -p jars

echo "Création des jars vers jars/"
# Crée le jar du programme printFile
jar cmf out/production/printFile/META-INF/MANIFEST.MF jars/printFile.jar -C out/production/printFile main.class -C libs .

# Crée le jar du programme imageToASCII
jar cmf out/production/imageToASCII/META-INF/MANIFEST.MF jars/imageToASCII.jar -C out/production/imageToASCII main.class -C libs .
