import org.apache.commons.cli.*;
import org.apache.tools.ant.DirectoryScanner;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Scanner;


/**
 * class principale
 * test : -preview --output test C:\Users\Adil\Desktop\petits_monstres\bleu.png C:\Users\Adil\Desktop\petits_monstres\rouge.png C:\Users\Adil\Desktop\petits_monstres\vert.png
 */


class main {
    public static void main(String[] arguments) {
        Options options = createOption();

        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse( options, arguments );

            String[] files = getListArgs(line.getArgs());

            verifyOutputFolder(line.getOptionValue("output"));

            int facteur = 1;
            if (line.getOptionValue("facteur") != null) {
                try {
                    facteur = Integer.parseInt(line.getOptionValue("facteur"));
                } catch(NumberFormatException exp) {
                    System.err.println("Le facteur d'agrandissement n'est pas valide");
                    System.exit(1);
                }
            }

            String[] convertedImage = convertImages(files, facteur);


            if (line.hasOption("preview")) {
                boolean okpreview = false;

                while (!okpreview) {
                    convertedImage = convertImages(files, facteur);
                    printTab(convertedImage);

                    boolean okchoix = false;
                    Scanner scanner = new Scanner(System.in);

                    while (!okchoix) {
                        System.out.print("Le facteur d'agrandissement convient-il ? [y/n] : ");

                        switch (scanner.next()) {
                            case "y":
                                okchoix = true;
                                okpreview = true;
                                break;

                            case "n":
                                okchoix = true;
                                boolean oknumber = false;

                                while (!oknumber) {
                                    System.out.print("Choisissez un nouveau facteur d'agrandissement : ");
                                    try {
                                        facteur = Integer.parseInt(scanner.next());
                                        oknumber = true;
                                    } catch (NumberFormatException exp) {
                                        System.out.println("Chiffre non valide.");
                                    }
                                }
                                break;
                            default:
                                System.out.println("Choix non valide.");
                                break;
                        }
                    }
                }
            } else {
                convertedImage = convertImages(files, facteur);
            }

            saveImages(convertedImage, files, line.getOptionValue("output"));
        }
        catch(ParseException | IOException exp ) {
            System.out.println( exp.getMessage() );
            formatter.printHelp( "ascii_convert [-preview] [--facteur ...] --output path Image1 Image2 ...", options );
        }
    }

    private static String[] getListArgs(String[] arguments) {
        ArrayList<String> files = new ArrayList<String>();
        DirectoryScanner ds = new DirectoryScanner();

        String absoluteParentDirectory;

        for (String argument : arguments) {
            absoluteParentDirectory = new File(new File(argument).getAbsolutePath()).getParent();

            ds.setIncludes(new String[]{new File(argument).getName()});
            ds.setBasedir(absoluteParentDirectory);
            ds.setCaseSensitive(true);
            ds.scan();

            for (String file: ds.getIncludedFiles()) {
                files.add(absoluteParentDirectory + "/" + file);
            }
        }

        String[] toReturn = new String[files.size()];
        files.toArray(toReturn);

        return toReturn;
    }

    /**
     * Sauvegarde les String[] convertedImages dans le dossier String output sous le nom : nom de l'image + .txt
     * @param convertedImages
     * @param args
     * @param output
     * @throws IOException
     */
    private static void saveImages(String[] convertedImages, String[] args, String output) throws IOException {
        for (int i = 0; i < convertedImages.length; i++) {
            String path = new File(args[i]).getName();
            BufferedWriter writer = new BufferedWriter(new FileWriter(output + "/" + path + ".txt"));
            writer.write(convertedImages[i]);

            writer.close();
        }
    }


    /**
     * verifyOutput vérifie que le dossier String output existe sinon il le créer
     * @param output
     */
    private static void verifyOutputFolder(String output) {
        File file = new File(output);

        if(!(file).exists()) {
            file.mkdir();
        }
    }

    /**
     * convertImage convertie les images dans String[] args en ASCII art coloré
     * @param args
     * @return String[] contenant le code ASCII de l'image
     */

    private static String[] convertImages(String[] args, int facteur) {
        String[] convertedImage = new String[args.length];
        BufferedImage[] image = getBufferedImage(args);

        for(int i = 0; i < image.length; i++) {
            convertedImage[i] = imageToAscii(image[i], facteur);
        }

        return convertedImage;
    }

    /**
     * getBufferedImage génére un objet BufferedImage[] et vérifie que les images existent
     *
     * @param args : liste des chemins des images
     * @return BufferedImage[] form String[] args
     */
    private static BufferedImage[] getBufferedImage(String[] args) {
        BufferedImage[] images = new BufferedImage[args.length];

        int idx = 0;


            while (idx < args.length) {
                try {
                    images[idx] = ImageIO.read(new File(args[idx]));

                } catch (IOException exp) {
                        System.err.println("Erreur : impossible de lire le fichier " + args[idx]);
                        //System.exit(1);
                }

                idx++;
            }

        return images;
    }

    /**
     * imageToAscii converti bufferedImage en texte coloré
     * @param bufferedImage
     * @return
     */
    private static String imageToAscii(BufferedImage bufferedImage, int facteur) {
        String ascii = "";
        int red, blue, green, alpha;
        Color c;

        int[][] rgbArray = new int[bufferedImage.getHeight()][bufferedImage.getWidth()];
        fillRGBArray(rgbArray, bufferedImage);

        for(int height = 0; height < rgbArray.length; height++) {
            for(int fh = 0; fh < facteur; fh++) {
                for (int width = 0; width < rgbArray[height].length; width++) {
                    for(int fw = 0; fw < facteur; fw++) {
                        if (rgbArray[height][width]>>24 == 0x00) {
                            ascii += "   ";
                        } else {
                            c = new Color(rgbArray[height][width]);
                            red = c.getRed();
                            blue = c.getBlue();
                            green = c.getGreen();
                            ascii += "\u001b[48;2;" + red + ";" + green + ";" + blue + "m   \033[0m";
                        }
                    }
                }
                ascii += "\n";
            }
        }
        return ascii;
    }

    /**
     * fillRGBArray rempli int[][] rgbArray par getRGB de chaque pixel
     * @param rgbArray
     * @param bufferedImage
     */
    private static void fillRGBArray(int[][] rgbArray, BufferedImage bufferedImage) {
        for(int height = 0; height < rgbArray.length; height++) {
            for (int width = 0; width < rgbArray[height].length; width++) {
                rgbArray[height][width] = bufferedImage.getRGB(width, height);
            }
        }
    }

    /**
     * printTab affiche les valeurs d'un tableau de String
     * @param tab
     */
    private static void printTab(String[] tab) {
        for (String s : tab) {
            System.out.println(s);
        }
    }


    /**
     * createOption cré les options relatives à la ligne de commande
     * @return objet Options contenant les options
     */
    private static Options createOption() {
        Option help = new Option( "help", "affiche cette aide" );

        Option preview = new Option( "preview", "affiche les images afin de vérifie le facteur d'agrandissement avant exportation" );

        Option output   = Option.builder().argName( "output" )
                .hasArg()
                .required()
                .desc("dossier de destination des fichiers" )
                .longOpt("output").build();

        Option facteur   = Option.builder().argName( "facteur" )
                .hasArg()
                .desc("facteur d'agrandissement de l'image" )
                .longOpt("facteur").build();

        Options options = new Options();

        options.addOption( facteur );
        options.addOption( output );
        options.addOption( preview );
        options.addOption( help );

        return options;
    }
}